# That’s Why We’ve Built Your Fast Track

Apart from a whole set of tools, generators and best practices we have two solutions immediately ready for use. The Vaizr Power Admin, specially tailored towards the professional power user. This is a model that only comes with a mandatory subscription. We believe that these kind of sophisticated tools are of no use when they are not assisted by trained and experienced personnel to get the best out of this tool.

The Vaizr REST API we have completely open sourced to give back on the community on which strong shoulders we build our solutions.
If Vaizr REST API is so great, why open source it rather than keep it proprietary? The answer is simpler than you might think:

We believe fast delivery and quality are key ingredient to the innovative products and technologies of the future. Attempts in this area are global and growing fast, but lacks standard openness. By sharing what we believe to be one of the best Rest API toolboxes in the world, we hope to create substantial foothold for the new open standard for exchanging information, knowledge, ideas and putting immediate connectivity into products.
